#include "./s21_grep.h"

int main(int argumentsCount, char** arguments) {
    InitGrepData();
    scriptName = DublicateString(arguments[0]);

    if (argumentsCount > 1) {
        ParseOption(arguments, argumentsCount);
        int startIndex = TakePattern(arguments, argumentsCount);
        TakeFilesPaths(arguments, argumentsCount, startIndex);
    }

    if (CheckArgumentsParse() && !activeFlags->err) {
        Grep();
    }

    FreeGrepData();
    return EXIT_VALUE;
}

void InitGrepData() {
    activeFlags = malloc(sizeof(ActiveFlags));

    activeFlags->c = false;
    activeFlags->err = false;
    activeFlags->h = false;
    activeFlags->i = false;
    activeFlags->l = false;
    activeFlags->n = false;
    activeFlags->o = false;
    activeFlags->s = false;
    activeFlags->v = false;

    filePaths = InitList();
    patterns = InitList();
    f_patterns = InitList();
}

bool CheckArgumentsParse() {
    bool result = false;
    if (GetListSize(filePaths) > 1
        && (GetListSize(patterns) > 1
        || GetListSize(f_patterns) > 1)) {
        result = true;
    } else if (!activeFlags->err) {
        PRINT_ERR_ARGUMENTS();
    }
    return result;
}

void FreeGrepData() {
    FreeLinkedList(filePaths);
    FreeLinkedList(patterns);
    FreeLinkedList(f_patterns);
    free(scriptName);
    free(activeFlags);
}

void ParseOption(char** arguments, int argumentsCount) {
    char flag = '\0';
    opterr = 0;
    while (!activeFlags->err
        && (flag = getopt(argumentsCount,
                        arguments,
                        "e:f:ivclnhso")) != -1) {
        SetActiveFlags(flag);
    }
}

void SetActiveFlags(char parsedFlag) {
    switch (parsedFlag) {
    case 'e':
        AddEnd(patterns, optarg);
        break;
    case 'f':
        TakeFilePattern(optarg);
        break;
    case 'i':
        activeFlags->i = true;
        break;
    case 'v':
        activeFlags->v = true;
        break;
    case 'c':
        activeFlags->c = true;
        break;
    case 'l':
        activeFlags->l = true;
        break;
    case 'n':
        activeFlags->n = true;
        break;
    case 'h':
        activeFlags->h = true;
        break;
    case 's':
        activeFlags->s = true;
        break;
    case 'o':
        activeFlags->o = true;
        break;
    case '?':
        activeFlags->err = true;
        PRINT_ERR_NOT_CORRECT_OPTION();
        break;
    default:
        break;
    }
}

int TakePattern(char** arguments, int argumentsCount) {
    int i = 1;
    if (GetListSize(patterns) == 1 && !(GetListSize(f_patterns) > 1)) {
        bool breakPoint = false;
        while (i < argumentsCount && !breakPoint) {
            if (arguments[i][0] != '-') {
                AddEnd(patterns, arguments[i]);
                breakPoint = true;
            }
            i++;
        }
    }
    return i;
}

void TakeFilesPaths(char** arguments, int argumentsCount, int index) {
    while (index < argumentsCount) {
        if (arguments[index][0] != '-'
            && strcmp(arguments[index - 1], "-e") != 0
            && strcmp(arguments[index - 1], "-f") != 0) {
            AddEnd(filePaths, arguments[index]);
        }
        index++;
    }
    if (GetListSize(filePaths) > 2) {
        activeFlags->h = true;
    }
}

void TakeFilePattern(char* filePath) {
    if (CheckIsFile(filePath) && !CheckIsDir(filePath)) {
        AddEnd(f_patterns, filePath);
    } else {
        activeFlags->err = true;
    }
}

bool CheckIsFile(char* path) {
    FILE* file = NULL;
    bool result = false;
    if ((file = fopen(path, "r")) != NULL) {
        result = true;
        fclose(file);
    } else {
        PRINT_ERR_FILE(path);
    }
    return result;
}

bool CheckIsDir(char* path) {
    bool result = false;
    DIR* directory = NULL;
    if ((directory = opendir(path)) != NULL) {
        result = true;
        PRINT_ERR_IS_DIR(path);
        closedir(directory);
    }
    return result;
}

void Grep() {
    LinkedList* currentFile = filePaths->next;
    while (currentFile != NULL) {
        char* filePath = currentFile->value;
        if (CheckIsFile(filePath) && !CheckIsDir(filePath)) {
            Scan(filePath);
            if (find) {
                Handler_L(filePath);
                Handler_C(filePath);
            }
        }
        currentFile = currentFile->next;
    }
}

void Scan(char* path) {
    LineBuffer* buffer = malloc(sizeof(LineBuffer));
    buffer->size = 0;
    FILE* file = NULL;
    bool isEnd = false;
    if ((file = fopen(path, "r")) != NULL) {
        while (!feof(file)) {
            GetLine(file, buffer);
            isEnd = CheckIsEnd(file);
            SearchMatches(buffer->line, path, isEnd);
            Handler_F(buffer->line, path, isEnd);
            if (!activeFlags->o) {
                PrintResult(buffer->line, path);
            }
            FreeBuffer(buffer);
            if (!activeFlags->l && !activeFlags->c) {
                find = false;
            }
            countLine++;
        }
        countLine = 1;
        fclose(file);
    }
    free(buffer);
}

regex_t CompileRegex(char* pattern) {
    regex_t result;
    int cflags = 0;
    if (strlen(pattern) == 0) {
        isEmptyLine = true;
    }
    char* currentPattern = DublicateString(pattern);
    if (activeFlags->i) {
        cflags = REG_ICASE;
    }
    if (activeFlags->v) {
        currentPattern = Handler_V(currentPattern);
    }
    regcomp(&result, currentPattern, cflags);
    free(currentPattern);
    return result;
}

void SearchMatches(char* line, char* filePath, bool isEnd) {
    if (GetListSize(patterns) > 1 && !isEnd) {
        LinkedList* currentPattern = patterns->next;
        regex_t preg;
        while (currentPattern != NULL) {
            preg = CompileRegex(currentPattern->value);
            SearchCurrentPattern(&preg, line, filePath);
            currentPattern = currentPattern->next;
        }
        regfree(&preg);
    }
}

void SearchCurrentPattern(regex_t* preg, char* line, char* filePath) {
    if (line != NULL) {
        regmatch_t pmatch;
        int match = -1;
        match = regexec(preg, line, 1, &pmatch, 0);
        if (match == 0) {
            if (!find) {
                find = true;
            }
            matched++;
            Handler_O(preg, &pmatch, line, filePath);
        }
    }
}

void PrintResult(char* line, char* filePath) {
    if (find && !activeFlags->l && !activeFlags->c) {
        Handler_H(filePath);
        Handler_N();
        printf("%s\n", line);
    }
}

bool CheckIsEnd(FILE* file) {
    bool result = false;
    if (feof(file)) {
        result = true;
    }
    return result;
}

void GetLine(FILE* file, LineBuffer* buffer) {
    char ch = '\0';
    while ((ch = fgetc(file)) != '\n'
        && ch != EOF
        && buffer->size < BUFSIZ) {
        buffer->line[buffer->size] = ch;
        buffer->size++;
    }
    buffer->line[buffer->size] = '\0';
    buffer->size++;
}

void FreeBuffer(LineBuffer* buffer) {
    for (int i = buffer->size; i >= 0; i--) {
        buffer->line[i] = '\0';
    }
    buffer->size = 0;
}

char* Handler_V(char* pattern) {
    char* patternV = malloc(strlen(pattern) * sizeof(char) + 10);
    patternV = strcpy(patternV, "[^");
    patternV = strcat(patternV, pattern);
    patternV = strcat(patternV, "]");
    free(pattern);
    return patternV;
}

void Handler_C(char* filePath) {
    if (activeFlags->c && !activeFlags->l) {
        Handler_H(filePath);
        printf("%d\n", matched);
        matched = 0;
    }
}

void Handler_L(char* filePath) {
    if (activeFlags->l) {
        printf("%s\n", filePath);
    }
}

void Handler_N() {
    if (activeFlags->n) {
        printf("%d:", countLine);
    }
}

void Handler_H(char* fileName) {
    if (activeFlags->h) {
        printf("%s:", fileName);
    }
}

void Handler_F(char* line, char* filePath, bool isEnd) {
    if (GetListSize(f_patterns) > 1 && !isEnd) {
        LinkedList* currentF_patterns = f_patterns->next;
        FILE* patternFile = NULL;
        while (currentF_patterns != NULL) {
            char* patternPath = currentF_patterns->value;
            if ((patternFile  = fopen(patternPath, "r")) != NULL) {
                F_Scan(line, patternFile, filePath);
                fclose(patternFile);
            }
            currentF_patterns = currentF_patterns->next;
        }
    }
}

void F_Scan(char* line, FILE* patternFile, char* filePath) {
    LineBuffer* f_buffer = malloc(sizeof(LineBuffer));
    f_buffer->size = 0;
    regex_t preg;
    while (!feof(patternFile)) {
        GetLine(patternFile, f_buffer);
        if (f_buffer->size == 0 && feof(patternFile)) {
        } else {
            preg = CompileRegex(f_buffer->line);
            SearchCurrentPattern(&preg, line, filePath);
            regfree(&preg);
        }
        FreeBuffer(f_buffer);
    }
    free(f_buffer);
}

void Handler_O(regex_t* preg, regmatch_t* pmatch, char* line, char* path) {
    if (activeFlags->o && !activeFlags->c && !activeFlags->l && !isEmptyLine) {
        int start = pmatch->rm_so;
        int end = pmatch->rm_eo;
        Handler_H(path);
        Handler_N();
        for (int i = start; i < end; i++) {
            printf("%c", line[i]);
        }
        printf("\n");
        if (line[end] != '\0') {
            SearchCurrentPattern(preg, line + end, path);
        }
    }
}

void PRINT_ERR_NOT_CORRECT_OPTION() {
    if (!activeFlags->s) {
        if (optopt == 'e') {
            printf("%s: No pattern after \"e\"\n", scriptName);
        } else if (optopt == 'f') {
            printf("%s: No file after \"f\"\n", scriptName);
        } else {
            printf("%s: Not correct option - \"%c\"\n",
                    scriptName, (char)optopt);
        }
    }
    EXIT_VALUE = 1;
}

void PRINT_ERR_ARGUMENTS() {
    printf("Use: %s [OPTIONS] [PATTERNS] [FILE]\n", scriptName);
    EXIT_VALUE = 1;
}

void PRINT_ERR_FILE(char* filePath) {
    if (!activeFlags->s) {
        printf("%s: %s: No such file or directory\n", scriptName, filePath);
    }
    EXIT_VALUE = 1;
}

void PRINT_ERR_IS_DIR(char* filePath) {
    if (!activeFlags->s) {
        printf("%s: %s: Is a directory\n", scriptName, filePath);
    }
    EXIT_VALUE = 1;
}
