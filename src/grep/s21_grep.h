#ifndef SRC_GREP_S21_GREP_H_
#define SRC_GREP_S21_GREP_H_

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <regex.h>
#include "../common/bool.h"
#include "../common/queue/queue.h"
#include "../common/linkedList/linkedList.h"

typedef struct ActiveFlags {
    bool i;
    bool v;
    bool c;
    bool l;
    bool n;
    bool h;
    bool s;
    bool o;
    bool err;
} ActiveFlags;

typedef struct LineBuffer {
    char line[BUFSIZ];
    int size;
} LineBuffer;

static int EXIT_VALUE = 0;

static char* scriptName = NULL;

static ActiveFlags* activeFlags = NULL;
static LinkedList* patterns = NULL;
static LinkedList* filePaths = NULL;
static LinkedList* f_patterns = NULL;

static int countLine = 1;
static int matched = 0;
static bool find = false;

static bool isEmptyLine = false;

void InitGrepData();
bool CheckArgumentsParse();
void FreeGrepData();

void ParseOption(char** arguments, int argumentsCount);
void SetActiveFlags(char parsedFlag);

int TakePattern(char** arguments, int argumentsCount);
void TakeFilesPaths(char** arguments, int argumentsCount, int index);
void TakeFilePattern(char* filePath);

bool CheckIsFile(char* path);
bool CheckIsDir(char* path);

void Grep();
void Scan(char* path);

regex_t CompileRegex(char* pattern);
void SearchMatches(char* line, char* filePath, bool isEnd);
void SearchCurrentPattern(regex_t* preg, char* line, char* filePath);
void PrintResult(char* line, char* filePath);

void GetLine(FILE* file, LineBuffer* buffer);
bool CheckIsEnd(FILE* file);
void FreeBuffer(LineBuffer* buffer);

char* Handler_V(char* pattern);
void Handler_C(char* filePath);
void Handler_L(char* filePath);
void Handler_N();
void Handler_H(char* fileName);

void Handler_F(char* line, char* filePath, bool isEnd);
void F_Scan(char* line, FILE* patternFile, char* filePath);
char* ReadFilePattern(FILE* file, LineBuffer* buffer);

void Handler_O(regex_t* preg, regmatch_t* pmatch, char* line, char* path);
char* SetOffset(char* line, int offset);

void PRINT_ERR_NOT_CORRECT_OPTION();
void PRINT_ERR_ARGUMENTS();
void PRINT_ERR_FILE(char* filePath);
void PRINT_ERR_IS_DIR(char* filePath);

#endif  //  SRC_GREP_S21_GREP_H_
